'use strict'

const EventEmitter = require('events');
const myEmitter = new EventEmitter();

let activeConnections = []
let connectionCount = 0

function ServerSentEvents () {
    return {
        eventPusher: eventPusher,
        addClient: addClient,
        activeConnectionsCounter: activeConnectionsCounter,
        getActiveConnection: getActiveConnection
    }
}

function initializeConnection(req, res) {
    console.log('init')
    req.socket.setTimeout(0);
    req.socket.setNoDelay(true);
    req.socket.setKeepAlive(true);
    res.set('Content-Type', 'text/event-stream')
    res.set('Cache-Control', 'no-cache')
    res.set('Connection', 'keep-alive')
    res.write('\n')
}

function addClient(req, res) {
    initializeConnection(req, res)
    activeConnections.push(res)
}

function eventPusher(options) {
    return function (req, res, next) {
        let allowedVerbs = false
        let includeResources = false
        let excludeResources = false

        options.allowedVerbs.forEach(function(verb) {
            if (req.method === verb) {
                allowedVerbs = true
            }
            
        }, this);
        options.includeResources.forEach(function(incResource) {
            if (req.url.includes(incResource)) {
                includeResources = true
            }
            
        }, this);
        options.excludeResources.forEach(function(excResource) {
            if (!req.url.includes(excResource)) {
                excludeResources = true
            }
            
        }, this);
        
        if (allowedVerbs && includeResources && excludeResources) {
            res.on('finish', function () {
                if (res.statusCode == 201 || res.statusCode == 200) {
                    console.log('haha made it through')
                    myEmitter.emit('trigger')
                }
            });
        }
      return next()
    }
}

myEmitter.on('trigger', () => {
    console.log('an event occurred!')
    activeConnections.forEach(function(client) {
        client.write('data: {\ndata: "msg": "hello world",\ndata: "id": 12345\ndata: }\n\n')    
    }, this);
})
    
// removeClient() {}


// connectionStatus() {}
    
function activeConnectionsCounter() {
    return activeConnections.length
}
function getActiveConnection() {
    return activeConnections
}
    
// messageSerializer() {}

// messageDeserializer() {}
module.exports.ServerSentEvents = ServerSentEvents